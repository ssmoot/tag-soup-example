# Overview

Basic example of parsing HTML from a URL in Scala.

To run the example: `sbt run`

Briefly: This shows you how to integrate with Scala's own XML library by
substituting a parser from the TagSoup HTML parsing library.

```
XML.withSAXParser(new org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl().newSAXParser())
```

After that, it's just standard Scala. `parser.load(someURL)` your content. Traverse it with
`\\` and `\`. Get text nodes with `.text()`. Not much else to it.