name := "tag-soup-example"

scalaVersion := "2.11.6"

version := "1.0-SNAPSHOT"

organization := "me.ssmoot"

scalacOptions := Seq("-deprecation", "-unchecked", "-feature", "-language:postfixOps", "-language:implicitConversions")

libraryDependencies ++= Seq(
  "org.ccil.cowan.tagsoup" % "tagsoup" % "1.2.1" % "compile",
  "org.scala-lang.modules" %% "scala-xml" % "1.0.1")
