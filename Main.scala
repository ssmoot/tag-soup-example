import scala.xml._

object Main extends App {

  val page = XML.withSAXParser(new org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl().newSAXParser())
               .load("http://www.nissan-newsroom.com/EN/PRODUCTS/OLDMODELS/list_a.html")

  (page \\ "h4") map(_.text) foreach println

}
